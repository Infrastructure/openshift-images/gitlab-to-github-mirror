FROM registry.access.redhat.com/ubi8/python-38 

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY mirror2github.py /usr/local/bin

ENTRYPOINT ["mirror2github.py"]

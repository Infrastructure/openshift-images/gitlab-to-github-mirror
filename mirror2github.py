#!/usr/bin/env python3
import os
import sys

import gitlab
import github
from github.GithubException import UnknownObjectException

def configure_mirrors(gl, gh):
    gh_org = gh.get_organization("GNOME")

    group_name = "GNOME"
    group = gl.groups.get(group_name, lazy=True)
    projects = group.projects.list(all=True)

    extra_projects = gl.projects.list(
        custom_attributes={"enable_github_mirror": "true"}
    )

    for prj in projects + extra_projects:
        # Skip project if it has been migrated from GNOME group
        prj = gl.projects.get(prj.id)
        if prj.namespace["name"] != group.id and prj not in extra_projects:
            continue

        # Skip projects without git repository
        if "default_branch" not in prj.attributes:
            continue

        mirror_settings = {
            "description": f"Read-only mirror of {prj.web_url}",
            "homepage": prj.web_url,
            "has_issues": False,
            "has_wiki": False,
        }

        try:
            gh_mirror = gh_org.get_repo(prj.path)
            gh_branches = [br.name for br in gh_mirror.get_branches()]

            if len(gh_branches):
                if (
                    prj.default_branch not in gh_branches
                    and prj.default_branch != gh_mirror.default_branch
                ):
                    gh_mirror.create_git_ref(
                        f"refs/heads/{prj.default_branch}",
                        gh_mirror.get_commit(
                            f"refs/heads/{gh_mirror.default_branch}"
                        ).sha,
                    )

                gh_mirror.edit(default_branch=prj.default_branch)

            gh_mirror.edit(**mirror_settings)
        except UnknownObjectException:
            print(404)
            gh_mirror = gh_org.create_repo(prj.path, **mirror_settings)

        ssh_url = f"ssh://git@github.com/GNOME/{prj.path}.git"

        mirrors = prj.remote_mirrors.list(all=True, lazy=True)
        mirrored = False
        for m in mirrors:
            if m.url == ssh_url:
                mirrored = True

        if not mirrored:
            prj.remote_mirrors.create({"url": ssh_url, "enabled": True})


def remove_deprecated_gh_repos(gl, gh):
    gh_org = gh.get_organization("GNOME")
    gh_repos = gh_org.get_repos()

    group_name = "GNOME"
    group = gl.groups.get(group_name, lazy=True)
    group_projects = group.projects.list(all=True, lazy=True)

    extra_projects = gl.projects.list(
        custom_attributes={"enable_github_mirror": "true"}, lazy=True
    )

    keeplist = ['stands-website']
    gl_projects = [prj.path for prj in group_projects + extra_projects] + keeplist
    orphan_mirrors = [mirror for mirror in gh_repos if mirror.name not in gl_projects]

    for mirror in orphan_mirrors:
        mirror.delete()


if __name__ == "__main__":
    gitlab_token = os.environ.get('GITLAB_RW_TOKEN')
    github_token = os.environ.get('GITHUB_OAUTH_TOKEN')

    gl = gitlab.Gitlab(
        "https://gitlab.gnome.org", private_token=gitlab_token, per_page=100
    )

    gh = github.Github(github_token)

    configure_mirrors(gl, gh)
    remove_deprecated_gh_repos(gl, gh)
